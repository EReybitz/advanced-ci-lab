# Introduction

The goal of this exercise is to implement parallel matrix jobs to reduce job definition redundancy. 

## Step 01 - Parallelize the build job

1. Instead of writing out separate instances of our build job, we will use the parallel matrix feature to run multiple instances of the same job with different variables.  
We will repurpose the **_.build_** job as our single build job.  

- Go to **Build > Pipeline Editor**.  
- First, rename **_.build_** to **_build_**, removing the "." in the front of the name so that it is no longer a hidden job.  
- Then modify the **_build_** job to remove the script line that echos the `IMAGE` variable to the `imagevars.env` file, and remove the `artifacts:reports:dotenv` report section.  

The build job should now look like this:
```plaintext
build:
  stage: build
  variables:
    IMAGE: $CI_REGISTRY_IMAGE/$PROJECT_DIR:$CI_COMMIT_SHA
  script:
    - cd $PROJECT_DIR
    - pwd
    - echo "building $PROJECT_DIR"
    - docker info
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $IMAGE .
    - docker push $IMAGE
```
2. Add the parallel:matrix section underneath the script block of the **_build_** job:
```plaintext
  parallel:
    matrix:
      - PROJECT_DIR: [python1, python2]
```

Your build job should now look like this:
```plaintext
build:
  stage: build
  variables:
    IMAGE: $CI_REGISTRY_IMAGE/$PROJECT_DIR:$CI_COMMIT_SHA
  script:
    - cd $PROJECT_DIR
    - pwd
    - echo "building $PROJECT_DIR"
    - docker info
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $IMAGE .
    - docker push $IMAGE
  parallel:
    matrix:
      - PROJECT_DIR: [python1, python2]
```
3. Delete the **_build1_** and **_build2_** jobs.  

**_Note: You will want to complete Step 02 below before committing your changes_**

## Step 02 - Parallelize the container test job
1. Delete the **_container1_job_** and **_container2_job_** jobs.    
2. Modify the **_.get_container_** job in a similar way to the **_build_** job above:
  - remove the "." at the front of the name so that it is no longer a hidden job
  - add the parallel:matrix block underneath the script block
```plaintext
  parallel:
    matrix:
      - PROJECT_DIR: [python1,python2]
```

3. Add a **_variables:_** section to the **_get_container_** job to explicitly define the `IMAGE` value  
```plaintext
  variables:
    IMAGE: $CI_REGISTRY_IMAGE/$PROJECT_DIR:$CI_COMMIT_SHA
```
The resulting **_get_container_** job should look like this:

```plaintext
get_container:
  stage: test
  variables:
    IMAGE: $CI_REGISTRY_IMAGE/$PROJECT_DIR:$CI_COMMIT_SHA
  script:
    - echo "The image being used is $IMAGE"
    # - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    # - docker pull $IMAGE 
  parallel:
    matrix:
      - PROJECT_DIR: [python1,python2]
```

Commit your changes and navigate to the running pipeline to view its details.

### _Need additional help with this exercise? _You can see the full .gitlab_ci.yml file solution for this exercise in the `3-parallel-matrix` branch._

## BONUS 
See how GitLab uses the `parallel:matrix` capability for our own [Workhorse testing](https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/ci/workhorse.gitlab-ci.yml#L32) -it's used to run tests for the workhorse code against multiple Go versions and Redis versions concurrently.

